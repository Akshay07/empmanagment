import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { RegistrationComponent } from './registration.component';
import { IonicModule } from '@ionic/angular';
import { FormsModule } from '@angular/forms';



@NgModule({
  declarations: [RegistrationComponent],
  imports: [
      CommonModule,
      FormsModule,
      IonicModule,
      RouterModule.forChild([
        {
          path: '',
          component: RegistrationComponent
        }
      ])
  ]
})
export class RegistrationModule { }
export class Manager {
  firstname: any;
  lastname : any;
  password : any;
  address : any;
  company : any;
  dob :any;
}
