import { Component, OnInit, ɵConsole } from '@angular/core';
import { Manager } from './registration.module';
@Component({
  selector: 'app-registration',
  templateUrl: './registration.component.html',
  styleUrls: ['./registration.component.scss'],
})
export class RegistrationComponent implements OnInit {
  dob : any;
  today : any;
  maxdate : any;
  manager : Manager;
  firstname: any;
  lastname: any;
  password: any;
  address: any;
  company: any;
  data : any = {};
  constructor() { }

  createManager(data){
    this.data.firstname = this.firstname;
    this.data.lastname = this.lastname;
    this.data.password = this.password;
    this.data.address = this.address;
    this.data.company = this.company;
    this.data.dob = this.dob;
    console.log("data object"+JSON.stringify(this.data));
  }

  onDateChange(){
    console.log("date = "+this.dob.split("T")[0]);//.split('-')[0];
    this.dob = this.dob.split("T")[0];
  }

  ngOnInit() {
    const now = new Date();
      this.today = now.toISOString();
      this.maxdate = this.today;
  }

}
